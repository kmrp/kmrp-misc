package ru.konungstvo.kmrpitems.procedure;

import ru.konungstvo.kmrpitems.ElementsKmrpitems2Mod;

import net.minecraft.world.World;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.math.Vec3d;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.ResourceLocation;
import net.minecraft.server.MinecraftServer;
import net.minecraft.scoreboard.Scoreboard;
import net.minecraft.scoreboard.ScoreObjective;
import net.minecraft.scoreboard.ScoreCriteria;
import net.minecraft.scoreboard.Score;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.Entity;
import net.minecraft.command.ICommandSender;

import java.util.Map;

@ElementsKmrpitems2Mod.ModElement.Tag
public class ProcedureMine3EntityCollidesInTheBlock extends ElementsKmrpitems2Mod.ModElement {
	public ProcedureMine3EntityCollidesInTheBlock(ElementsKmrpitems2Mod instance) {
		super(instance, 27);
	}

	public static void executeProcedure(Map<String, Object> dependencies) {
		if (dependencies.get("entity") == null) {
			System.err.println("Failed to load dependency entity for procedure Mine3EntityCollidesInTheBlock!");
			return;
		}
		if (dependencies.get("x") == null) {
			System.err.println("Failed to load dependency x for procedure Mine3EntityCollidesInTheBlock!");
			return;
		}
		if (dependencies.get("y") == null) {
			System.err.println("Failed to load dependency y for procedure Mine3EntityCollidesInTheBlock!");
			return;
		}
		if (dependencies.get("z") == null) {
			System.err.println("Failed to load dependency z for procedure Mine3EntityCollidesInTheBlock!");
			return;
		}
		if (dependencies.get("world") == null) {
			System.err.println("Failed to load dependency world for procedure Mine3EntityCollidesInTheBlock!");
			return;
		}
		Entity entity = (Entity) dependencies.get("entity");
		int x = (int) dependencies.get("x");
		int y = (int) dependencies.get("y");
		int z = (int) dependencies.get("z");
		World world = (World) dependencies.get("world");
		if ((entity instanceof EntityPlayer)) {
			if ((!((entity instanceof EntityPlayer) ? ((EntityPlayer) entity).capabilities.isCreativeMode : false))) {
				if (entity instanceof EntityPlayer && !entity.world.isRemote) {
					((EntityPlayer) entity).sendStatusMessage(new TextComponentString(
							"\u0412\u044B \u043D\u0430\u0441\u0442\u0443\u043F\u0438\u043B\u0438 \u043D\u0430 \u043C\u0438\u043D\u0443. \u0417\u043E\u0432\u0438 \u0413\u041C."),
							(true));
				}
				System.out.println((((entity.getDisplayName().getUnformattedText())) + "" + (" has stepped on a mine at ") + "" + ("x:") + "" + (x)
						+ "" + (" y:") + "" + (y) + "" + (" z:") + "" + (z)));
				entity.setInWeb();
				{
					Entity _ent = entity;
					if (!_ent.world.isRemote && _ent.world.getMinecraftServer() != null) {
						_ent.world.getMinecraftServer().getCommandManager().executeCommand(new ICommandSender() {
							@Override
							public String getName() {
								return "";
							}

							@Override
							public boolean canUseCommand(int permission, String command) {
								return true;
							}

							@Override
							public World getEntityWorld() {
								return _ent.world;
							}

							@Override
							public MinecraftServer getServer() {
								return _ent.world.getMinecraftServer();
							}

							@Override
							public boolean sendCommandFeedback() {
								return false;
							}

							@Override
							public BlockPos getPosition() {
								return _ent.getPosition();
							}

							@Override
							public Vec3d getPositionVector() {
								return new Vec3d(_ent.posX, _ent.posY, _ent.posZ);
							}

							@Override
							public Entity getCommandSenderEntity() {
								return _ent;
							}
						}, "help");
					}
				}
				world.playSound((EntityPlayer) null, x, y, z, (net.minecraft.util.SoundEvent) net.minecraft.util.SoundEvent.REGISTRY
						.getObject(new ResourceLocation("block.dispenser.launch")), SoundCategory.NEUTRAL, (float) 1, (float) 1);
				if (entity instanceof EntityPlayer) {
					Scoreboard _sc = ((EntityPlayer) entity).getWorldScoreboard();
					ScoreObjective _so = _sc.getObjective("mines_triggered");
					if (_so == null) {
						_so = _sc.addScoreObjective("mines_triggered", ScoreCriteria.DUMMY);
					}
					Score _scr = _sc.getOrCreateScore(((EntityPlayer) entity).getGameProfile().getName(), _so);
					_scr.setScorePoints((int) ((new Object() {
						public int getScore(String score) {
							if (entity instanceof EntityPlayer) {
								Scoreboard _sc = ((EntityPlayer) entity).getWorldScoreboard();
								ScoreObjective _so = _sc.getObjective(score);
								if (_so != null) {
									Score _scr = _sc.getOrCreateScore(((EntityPlayer) entity).getGameProfile().getName(), _so);
									return _scr.getScorePoints();
								}
							}
							return 0;
						}
					}.getScore("mines_triggered")) + 1));
				}
				if (!world.isRemote && world.getMinecraftServer() != null) {
					world.getMinecraftServer().getCommandManager().executeCommand(new ICommandSender() {
						@Override
						public String getName() {
							return "";
						}

						@Override
						public boolean canUseCommand(int permission, String command) {
							return true;
						}

						@Override
						public World getEntityWorld() {
							return world;
						}

						@Override
						public MinecraftServer getServer() {
							return world.getMinecraftServer();
						}

						@Override
						public boolean sendCommandFeedback() {
							return false;
						}

						@Override
						public BlockPos getPosition() {
							return new BlockPos((int) x, (int) y, (int) z);
						}

						@Override
						public Vec3d getPositionVector() {
							return new Vec3d(x, y, z);
						}
					}, (("kill") + "" + ((entity.getDisplayName().getUnformattedText()))));
				}
				world.setBlockToAir(new BlockPos((int) x, (int) y, (int) z));
			}
		} else {
			if ((entity instanceof EntityLiving)) {
				entity.setInWeb();
				world.playSound((EntityPlayer) null, x, y, z, (net.minecraft.util.SoundEvent) net.minecraft.util.SoundEvent.REGISTRY
						.getObject(new ResourceLocation("block.dispenser.launch")), SoundCategory.NEUTRAL, (float) 1, (float) 1);
				if (!world.isRemote && world.getMinecraftServer() != null) {
					world.getMinecraftServer().getCommandManager().executeCommand(new ICommandSender() {
						@Override
						public String getName() {
							return "";
						}

						@Override
						public boolean canUseCommand(int permission, String command) {
							return true;
						}

						@Override
						public World getEntityWorld() {
							return world;
						}

						@Override
						public MinecraftServer getServer() {
							return world.getMinecraftServer();
						}

						@Override
						public boolean sendCommandFeedback() {
							return false;
						}

						@Override
						public BlockPos getPosition() {
							return new BlockPos((int) x, (int) y, (int) z);
						}

						@Override
						public Vec3d getPositionVector() {
							return new Vec3d(x, y, z);
						}
					}, (("kill") + "" + ((entity.getDisplayName().getUnformattedText()))));
				}
				world.setBlockToAir(new BlockPos((int) x, (int) y, (int) z));
			}
		}
	}
}
