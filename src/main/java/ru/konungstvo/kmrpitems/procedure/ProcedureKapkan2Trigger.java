package ru.konungstvo.kmrpitems.procedure;

import ru.konungstvo.kmrpitems.ElementsKmrpitems2Mod;

import net.minecraft.world.World;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.math.Vec3d;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.ResourceLocation;
import net.minecraft.server.MinecraftServer;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.Entity;
import net.minecraft.command.ICommandSender;

import java.util.Map;

@ElementsKmrpitems2Mod.ModElement.Tag
public class ProcedureKapkan2Trigger extends ElementsKmrpitems2Mod.ModElement {
	public ProcedureKapkan2Trigger(ElementsKmrpitems2Mod instance) {
		super(instance, 45);
	}

	public static void executeProcedure(Map<String, Object> dependencies) {
		if (dependencies.get("entity") == null) {
			System.err.println("Failed to load dependency entity for procedure Kapkan2Trigger!");
			return;
		}
		if (dependencies.get("x") == null) {
			System.err.println("Failed to load dependency x for procedure Kapkan2Trigger!");
			return;
		}
		if (dependencies.get("y") == null) {
			System.err.println("Failed to load dependency y for procedure Kapkan2Trigger!");
			return;
		}
		if (dependencies.get("z") == null) {
			System.err.println("Failed to load dependency z for procedure Kapkan2Trigger!");
			return;
		}
		if (dependencies.get("world") == null) {
			System.err.println("Failed to load dependency world for procedure Kapkan2Trigger!");
			return;
		}
		Entity entity = (Entity) dependencies.get("entity");
		int x = (int) dependencies.get("x");
		int y = (int) dependencies.get("y");
		int z = (int) dependencies.get("z");
		World world = (World) dependencies.get("world");
		if ((entity instanceof EntityPlayer)) {
			if ((!((entity instanceof EntityPlayer) ? ((EntityPlayer) entity).capabilities.isCreativeMode : false))) {
				if (entity instanceof EntityPlayer && !entity.world.isRemote) {
					((EntityPlayer) entity).sendStatusMessage(new TextComponentString(
							"\u0412\u044B \u043F\u043E\u043F\u0430\u043B\u0438 \u0432 \u043A\u0430\u043F\u043A\u0430\u043D. \u0417\u043E\u0432\u0438 \u0413\u041C."),
							(true));
				}
				if (entity instanceof EntityPlayer && !entity.world.isRemote) {
					((EntityPlayer) entity).sendStatusMessage(new TextComponentString(
							"\u00A74\u0412\u044B \u043F\u043E\u043F\u0430\u043B\u0438 \u0432 \u043A\u0430\u043F\u043A\u0430\u043D. \u0417\u043E\u0432\u0438 \u0413\u041C."),
							(false));
				}
				entity.setInWeb();
				if (!world.isRemote && world.getMinecraftServer() != null) {
					world.getMinecraftServer().getCommandManager().executeCommand(new ICommandSender() {
						@Override
						public String getName() {
							return "";
						}

						@Override
						public boolean canUseCommand(int permission, String command) {
							return true;
						}

						@Override
						public World getEntityWorld() {
							return world;
						}

						@Override
						public MinecraftServer getServer() {
							return world.getMinecraftServer();
						}

						@Override
						public boolean sendCommandFeedback() {
							return false;
						}

						@Override
						public BlockPos getPosition() {
							return new BlockPos((int) x, (int) y, (int) z);
						}

						@Override
						public Vec3d getPositionVector() {
							return new Vec3d(x, y, z);
						}
					}, (("dmsggmmine") + "" + (" ") + "" + ("\u043A\u0430\u043F\u043A\u0430\u043D_\u0443\u0440\u043E\u0432\u043D\u044F_2") + ""
							+ (" ") + "" + (x) + "" + (" ") + "" + (y) + "" + (" ") + "" + (z) + "" + (" ") + ""
							+ ((entity.getDisplayName().getUnformattedText()))));
				}
				world.setBlockToAir(new BlockPos((int) x, (int) y, (int) z));
				world.playSound((EntityPlayer) null, x, y, z,
						(net.minecraft.util.SoundEvent) net.minecraft.util.SoundEvent.REGISTRY.getObject(new ResourceLocation("item.shield.break")),
						SoundCategory.NEUTRAL, (float) 1, (float) 1);
			}
		} else {
			if ((entity instanceof EntityLiving)) {
				entity.setInWeb();
				if (!world.isRemote && world.getMinecraftServer() != null) {
					world.getMinecraftServer().getCommandManager().executeCommand(new ICommandSender() {
						@Override
						public String getName() {
							return "";
						}

						@Override
						public boolean canUseCommand(int permission, String command) {
							return true;
						}

						@Override
						public World getEntityWorld() {
							return world;
						}

						@Override
						public MinecraftServer getServer() {
							return world.getMinecraftServer();
						}

						@Override
						public boolean sendCommandFeedback() {
							return false;
						}

						@Override
						public BlockPos getPosition() {
							return new BlockPos((int) x, (int) y, (int) z);
						}

						@Override
						public Vec3d getPositionVector() {
							return new Vec3d(x, y, z);
						}
					}, (("dmsggmmine") + "" + (" ") + "" + ("\u043A\u0430\u043F\u043A\u0430\u043D_\u0443\u0440\u043E\u0432\u043D\u044F_2") + ""
							+ (" ") + "" + (x) + "" + (" ") + "" + (y) + "" + (" ") + "" + (z) + "" + (" ") + ""
							+ ((entity.getDisplayName().getUnformattedText()))));
				}
				world.playSound((EntityPlayer) null, x, y, z,
						(net.minecraft.util.SoundEvent) net.minecraft.util.SoundEvent.REGISTRY.getObject(new ResourceLocation("item.shield.break")),
						SoundCategory.NEUTRAL, (float) 1, (float) 1);
				world.setBlockToAir(new BlockPos((int) x, (int) y, (int) z));
			}
		}
	}
}
