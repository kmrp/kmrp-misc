
package ru.konungstvo.kmrpitems.creativetab;

import ru.konungstvo.kmrpitems.ElementsKmrpitems2Mod;

import net.minecraftforge.fml.relauncher.SideOnly;
import net.minecraftforge.fml.relauncher.Side;

import net.minecraft.item.ItemStack;
import net.minecraft.init.Items;
import net.minecraft.creativetab.CreativeTabs;

@ElementsKmrpitems2Mod.ModElement.Tag
public class TabKmrpmisc extends ElementsKmrpitems2Mod.ModElement {
	public TabKmrpmisc(ElementsKmrpitems2Mod instance) {
		super(instance, 16);
	}

	@Override
	public void initElements() {
		tab = new CreativeTabs("tabkmrpmisc") {
			@SideOnly(Side.CLIENT)
			@Override
			public ItemStack getTabIconItem() {
				return new ItemStack(Items.BAKED_POTATO, (int) (1));
			}

			@SideOnly(Side.CLIENT)
			public boolean hasSearchBar() {
				return false;
			}
		};
	}
	public static CreativeTabs tab;
}
