// Made with Blockbench 3.7.4
// Exported for Minecraft version 1.15
// Paste this class into your mod and generate all required imports

public static class Modeltitushat extends EntityModel<Entity> {
	private final ModelRenderer everything;
	private final ModelRenderer bone;
	private final ModelRenderer cube_r1;
	private final ModelRenderer cube_r2;
	private final ModelRenderer cube_r3;
	private final ModelRenderer cube_r4;
	private final ModelRenderer cube_r5;private final ModelRenderer 123;
	private final ModelRenderer cube_r6;
	private final ModelRenderer cube_r7;
	private final ModelRenderer cube_r8;
	private final ModelRenderer cube_r9;
	private final ModelRenderer cube_r10;
	private final ModelRenderer cube_r11;
	private final ModelRenderer cube_r12;
	private final ModelRenderer cube_r13;
	private final ModelRenderer cube_r14;
	private final ModelRenderer cube_r15;
	private final ModelRenderer cube_r16;
	private final ModelRenderer cube_r17;
	private final ModelRenderer cube_r18;
	private final ModelRenderer cube_r19;
	private final ModelRenderer cube_r20;
	private final ModelRenderer cube_r21;private final ModelRenderer 234;
	private final ModelRenderer cube_r22;
	private final ModelRenderer cube_r23;
	private final ModelRenderer cube_r24;
	private final ModelRenderer cube_r25;
	private final ModelRenderer cube_r26;
	private final ModelRenderer cube_r27;
	private final ModelRenderer cube_r28;
	private final ModelRenderer cube_r29;
	private final ModelRenderer cube_r30;
	private final ModelRenderer cube_r31;
	private final ModelRenderer cube_r32;private final ModelRenderer 345;
	private final ModelRenderer cube_r33;private final ModelRenderer 3453543;
	private final ModelRenderer cube_r34;
	private final ModelRenderer cube_r35;
	private final ModelRenderer cube_r36;
	private final ModelRenderer cube_r37;
	private final ModelRenderer cube_r38;
	private final ModelRenderer cube_r39;
	private final ModelRenderer cube_r40;
	private final ModelRenderer cube_r41;
	private final ModelRenderer bone2;

	public Modeltitushat() {
		textureWidth = 16;
		textureHeight = 16;

		everything = new ModelRenderer(this);
		everything.setRotationPoint(0.0F, 24.0F, 0.0F);
		

		bone = new ModelRenderer(this);
		bone.setRotationPoint(0.0F, 0.0F, 0.0F);
		everything.addChild(bone);
		bone.setTextureOffset(0, 9).addBox(-5.3754F, -13.3F, 4.9481F, 2.0F, 3.0F, 0.0F, 0.0F, false);
		bone.setTextureOffset(0, 9).addBox(-12.9754F, -13.3F, 4.9481F, 2.0F, 3.0F, 0.0F, 0.0F, false);
		bone.setTextureOffset(0, 9).addBox(-6.3754F, -13.3F, 4.9981F, 4.0F, 3.0F, 0.0F, 0.0F, false);
		bone.setTextureOffset(0, 9).addBox(-13.9754F, -13.3F, 4.9981F, 4.0F, 3.0F, 0.0F, 0.0F, false);

		cube_r1 = new ModelRenderer(this);
		cube_r1.setRotationPoint(-3.5704F, -7.283F, 2.617F);
		bone.addChild(cube_r1);
		setRotationAngle(cube_r1, 0.0F, -0.7854F, 0.0F);
		cube_r1.setTextureOffset(14, 4).addBox(-1.25F, -1.525F, 0.1F, 0.0F, 3.0F, 0.0F, 0.0F, true);
		cube_r1.setTextureOffset(14, 4).addBox(-0.65F, -1.525F, 0.1F, 0.0F, 3.0F, 0.0F, 0.0F, true);
		cube_r1.setTextureOffset(14, 4).addBox(-0.05F, -1.525F, 0.1F, 0.0F, 3.0F, 0.0F, 0.0F, true);
		cube_r1.setTextureOffset(14, 4).addBox(0.45F, -1.525F, 0.1F, 0.0F, 3.0F, 0.0F, 0.0F, true);
		cube_r1.setTextureOffset(14, 4).addBox(-1.55F, 1.075F, 0.1F, 3.0F, 0.0F, 0.0F, 0.0F, true);
		cube_r1.setTextureOffset(14, 4).addBox(-1.55F, 0.475F, 0.1F, 3.0F, 0.0F, 0.0F, 0.0F, true);
		cube_r1.setTextureOffset(14, 4).addBox(-1.55F, -0.625F, 0.1F, 3.0F, 0.0F, 0.0F, 0.0F, true);
		cube_r1.setTextureOffset(14, 4).addBox(-1.55F, -0.125F, 0.1F, 3.0F, 0.0F, 0.0F, 0.0F, true);
		cube_r1.setTextureOffset(14, 4).addBox(1.05F, -1.525F, 0.1F, 0.0F, 3.0F, 0.0F, 0.0F, true);
		cube_r1.setTextureOffset(14, 4).addBox(-1.55F, -1.225F, 0.1F, 3.0F, 0.0F, 0.0F, 0.0F, true);
		cube_r1.setTextureOffset(14, 4).addBox(-1.85F, 1.575F, 0.1F, 3.0F, 0.0F, 0.0F, 0.0F, false);
		cube_r1.setTextureOffset(14, 4).addBox(1.55F, -1.525F, 0.1F, 0.0F, 3.0F, 0.0F, 0.0F, false);
		cube_r1.setTextureOffset(14, 4).addBox(-1.85F, -1.825F, 0.1F, 3.0F, 0.0F, 0.0F, 0.0F, false);
		cube_r1.setTextureOffset(14, 4).addBox(-1.85F, -1.525F, 0.1F, 0.0F, 3.0F, 0.0F, 0.0F, false);

		cube_r2 = new ModelRenderer(this);
		cube_r2.setRotationPoint(-12.4296F, -7.283F, 2.617F);
		bone.addChild(cube_r2);
		setRotationAngle(cube_r2, 0.0F, 0.7854F, 0.0F);
		cube_r2.setTextureOffset(14, 4).addBox(-0.05F, -1.525F, -0.1F, 0.0F, 3.0F, 0.0F, 0.0F, false);
		cube_r2.setTextureOffset(14, 4).addBox(-1.65F, -0.125F, -0.1F, 3.0F, 0.0F, 0.0F, 0.0F, false);
		cube_r2.setTextureOffset(14, 4).addBox(-1.65F, 0.475F, -0.1F, 3.0F, 0.0F, 0.0F, 0.0F, false);
		cube_r2.setTextureOffset(14, 4).addBox(-1.65F, 1.075F, -0.1F, 3.0F, 0.0F, 0.0F, 0.0F, false);
		cube_r2.setTextureOffset(14, 4).addBox(-1.65F, -0.625F, -0.1F, 3.0F, 0.0F, 0.0F, 0.0F, false);
		cube_r2.setTextureOffset(14, 4).addBox(-1.65F, -1.225F, -0.1F, 3.0F, 0.0F, 0.0F, 0.0F, false);
		cube_r2.setTextureOffset(14, 4).addBox(1.05F, -1.525F, -0.1F, 0.0F, 3.0F, 0.0F, 0.0F, false);
		cube_r2.setTextureOffset(14, 4).addBox(0.45F, -1.525F, -0.1F, 0.0F, 3.0F, 0.0F, 0.0F, false);
		cube_r2.setTextureOffset(14, 4).addBox(-0.65F, -1.525F, -0.1F, 0.0F, 3.0F, 0.0F, 0.0F, false);
		cube_r2.setTextureOffset(14, 4).addBox(-1.25F, -1.525F, -0.1F, 0.0F, 3.0F, 0.0F, 0.0F, false);
		cube_r2.setTextureOffset(14, 4).addBox(-1.85F, -1.525F, -0.1F, 0.0F, 3.0F, 0.0F, 0.0F, false);
		cube_r2.setTextureOffset(14, 4).addBox(1.55F, -1.525F, -0.1F, 0.0F, 3.0F, 0.0F, 0.0F, false);
		cube_r2.setTextureOffset(14, 4).addBox(-1.85F, 1.575F, -0.1F, 3.0F, 0.0F, 0.0F, 0.0F, false);
		cube_r2.setTextureOffset(14, 4).addBox(-1.85F, -1.825F, -0.1F, 3.0F, 0.0F, 0.0F, 0.0F, false);

		cube_r3 = new ModelRenderer(this);
		cube_r3.setRotationPoint(-6.3883F, -8.008F, 3.3347F);
		bone.addChild(cube_r3);
		setRotationAngle(cube_r3, 0.0F, -0.7854F, 0.0F);
		cube_r3.setTextureOffset(14, 4).addBox(-0.465F, -1.2F, -2.2F, 3.0F, 3.0F, 0.0F, 0.0F, false);

		cube_r4 = new ModelRenderer(this);
		cube_r4.setRotationPoint(-9.6117F, -8.008F, 3.3347F);
		bone.addChild(cube_r4);
		setRotationAngle(cube_r4, 0.0F, 0.7854F, 0.0F);
		cube_r4.setTextureOffset(14, 4).addBox(-3.435F, -1.2F, -2.4F, 3.0F, 3.0F, 0.0F, 0.0F, false);
		cube_r4.setTextureOffset(7, 10).addBox(-3.335F, -1.1F, -2.425F, 3.0F, 3.0F, 1.0F, 0.0F, false);

		cube_r5 = new ModelRenderer(this);
		cube_r5.setRotationPoint(-6.3883F, -8.008F, 3.3347F);
		bone.addChild(cube_r5);
		setRotationAngle(cube_r5, 0.0F, -0.7854F, 0.0F);
		cube_r5.setTextureOffset(7, 10).addBox(-0.365F, -1.1F, -2.225F, 3.0F, 3.0F, 1.0F, 0.0F, false);

		123 = new ModelRenderer(this);
		123.setRotationPoint(-8.0F, -8.0F, 8.0F);
		everything.addChild(123);
		123.setTextureOffset(5, 13).addBox(-3.235F, -2.0F, -3.0F, 6.0F, 5.0F, 1.0F, 0.0F, false);
		123.setTextureOffset(5, 13).addBox(-2.135F, 2.7128F, -2.7818F, 4.0F, 0.0F, 0.0F, 0.0F, false);
		123.setTextureOffset(5, 13).addBox(-2.135F, -0.608F, -4.6991F, 4.0F, 4.0F, 0.0F, 0.0F, false);
		123.setTextureOffset(12, 6).addBox(-1.7073F, 0.4579F, -4.9991F, 0.0F, 1.0F, 0.0F, 0.0F, true);
		123.setTextureOffset(12, 6).addBox(1.1936F, 0.4402F, -4.9991F, 0.0F, 1.0F, 0.0F, 0.0F, true);
		123.setTextureOffset(12, 6).addBox(-0.9331F, -0.3164F, -4.9991F, 1.0F, 0.0F, 0.0F, 0.0F, true);
		123.setTextureOffset(12, 6).addBox(-1.2331F, 0.1086F, -4.8491F, 2.0F, 2.0F, 0.0F, 0.0F, true);
		123.setTextureOffset(12, 6).addBox(-0.9331F, 2.6022F, -4.9991F, 1.0F, 0.0F, 0.0F, 0.0F, true);

		cube_r6 = new ModelRenderer(this);
		cube_r6.setRotationPoint(-1.6117F, -0.008F, -4.6653F);
		123.addChild(cube_r6);
		setRotationAngle(cube_r6, 0.0F, 0.7854F, 0.0F);
		cube_r6.setTextureOffset(12, 6).addBox(-2.335F, -0.1F, -1.0F, 1.0F, 1.0F, 2.0F, 0.0F, true);

		cube_r7 = new ModelRenderer(this);
		cube_r7.setRotationPoint(1.6117F, -0.008F, -4.6653F);
		123.addChild(cube_r7);
		setRotationAngle(cube_r7, 0.0F, -0.7854F, 0.0F);
		cube_r7.setTextureOffset(12, 6).addBox(0.635F, -0.1F, -1.0F, 1.0F, 1.0F, 2.0F, 0.0F, true);

		cube_r8 = new ModelRenderer(this);
		cube_r8.setRotationPoint(-0.5F, -2.1F, -2.5F);
		123.addChild(cube_r8);
		setRotationAngle(cube_r8, -0.7854F, 0.0F, 0.0F);
		cube_r8.setTextureOffset(5, 13).addBox(-1.635F, 0.1F, -0.5F, 4.0F, 2.0F, 1.0F, 0.0F, false);

		cube_r9 = new ModelRenderer(this);
		cube_r9.setRotationPoint(-2.7426F, 3.0016F, -3.2141F);
		123.addChild(cube_r9);
		setRotationAngle(cube_r9, 0.0F, 0.0F, 0.7854F);
		cube_r9.setTextureOffset(5, 13).addBox(-0.335F, -0.5F, -0.135F, 1.0F, 0.0F, 1.0F, 0.0F, false);

		cube_r10 = new ModelRenderer(this);
		cube_r10.setRotationPoint(2.7426F, 3.0016F, -3.2141F);
		123.addChild(cube_r10);
		setRotationAngle(cube_r10, 0.0F, 0.0F, -0.7854F);
		cube_r10.setTextureOffset(5, 13).addBox(-1.135F, -0.5F, -0.135F, 1.0F, 0.0F, 1.0F, 0.0F, false);

		cube_r11 = new ModelRenderer(this);
		cube_r11.setRotationPoint(-0.5F, 4.3078F, -1.1818F);
		123.addChild(cube_r11);
		setRotationAngle(cube_r11, -0.3927F, 0.0F, 0.0F);
		cube_r11.setTextureOffset(5, 13).addBox(-1.635F, -0.5F, -3.6F, 4.0F, 1.0F, 2.0F, 0.0F, false);

		cube_r12 = new ModelRenderer(this);
		cube_r12.setRotationPoint(-0.0071F, 1.3806F, -4.8491F);
		123.addChild(cube_r12);
		setRotationAngle(cube_r12, 0.0F, 0.0F, -0.7854F);
		cube_r12.setTextureOffset(12, 6).addBox(1.3675F, -0.5324F, -0.15F, 0.0F, 1.0F, 0.0F, 0.0F, true);

		cube_r13 = new ModelRenderer(this);
		cube_r13.setRotationPoint(0.0022F, 1.3964F, -4.8491F);
		123.addChild(cube_r13);
		setRotationAngle(cube_r13, 0.0F, 0.0F, -0.7854F);
		cube_r13.setTextureOffset(12, 6).addBox(-1.8675F, -0.5501F, -0.15F, 0.0F, 1.0F, 0.0F, 0.0F, true);

		cube_r14 = new ModelRenderer(this);
		cube_r14.setRotationPoint(1.8809F, 3.8553F, -4.8491F);
		123.addChild(cube_r14);
		setRotationAngle(cube_r14, 0.0F, 0.0F, 0.7854F);
		cube_r14.setTextureOffset(12, 6).addBox(-1.7F, -0.935F, -0.15F, 0.0F, 1.0F, 0.0F, 0.0F, true);

		cube_r15 = new ModelRenderer(this);
		cube_r15.setRotationPoint(-0.3921F, 1.5469F, -4.8491F);
		123.addChild(cube_r15);
		setRotationAngle(cube_r15, 0.0F, 0.0F, 0.7854F);
		cube_r15.setTextureOffset(12, 6).addBox(-1.7F, -0.935F, -0.15F, 0.0F, 1.0F, 0.0F, 0.0F, true);

		cube_r16 = new ModelRenderer(this);
		cube_r16.setRotationPoint(-1.5501F, -0.0792F, -2.5F);
		123.addChild(cube_r16);
		setRotationAngle(cube_r16, 0.0F, 0.0F, -0.3927F);
		cube_r16.setTextureOffset(5, 13).addBox(-2.735F, -0.5F, -0.5F, 1.0F, 2.0F, 1.0F, 0.0F, false);

		cube_r17 = new ModelRenderer(this);
		cube_r17.setRotationPoint(-0.8995F, -0.4914F, -2.5F);
		123.addChild(cube_r17);
		setRotationAngle(cube_r17, 0.0F, 0.0F, 0.3927F);
		cube_r17.setTextureOffset(5, 13).addBox(-2.735F, -0.5F, -0.5F, 1.0F, 2.0F, 1.0F, 0.0F, false);

		cube_r18 = new ModelRenderer(this);
		cube_r18.setRotationPoint(1.5501F, -0.0792F, -2.5F);
		123.addChild(cube_r18);
		setRotationAngle(cube_r18, 0.0F, 0.0F, 0.3927F);
		cube_r18.setTextureOffset(5, 13).addBox(1.665F, -0.5F, -0.5F, 1.0F, 2.0F, 1.0F, 0.0F, false);

		cube_r19 = new ModelRenderer(this);
		cube_r19.setRotationPoint(0.8995F, -0.4914F, -2.5F);
		123.addChild(cube_r19);
		setRotationAngle(cube_r19, 0.0F, 0.0F, -0.3927F);
		cube_r19.setTextureOffset(5, 13).addBox(1.665F, -0.5F, -0.5F, 1.0F, 2.0F, 1.0F, 0.0F, false);

		cube_r20 = new ModelRenderer(this);
		cube_r20.setRotationPoint(-3.6447F, -0.208F, -2.4824F);
		123.addChild(cube_r20);
		setRotationAngle(cube_r20, 0.0F, -0.7854F, 0.0F);
		cube_r20.setTextureOffset(5, 13).addBox(-0.5F, -0.4F, -2.635F, 2.0F, 3.0F, 2.0F, 0.0F, false);

		cube_r21 = new ModelRenderer(this);
		cube_r21.setRotationPoint(1.6117F, -0.208F, -4.5153F);
		123.addChild(cube_r21);
		setRotationAngle(cube_r21, 0.0F, -0.7854F, 0.0F);
		cube_r21.setTextureOffset(5, 13).addBox(0.24F, -0.4F, -0.5F, 2.0F, 3.0F, 2.0F, 0.0F, false);

		234 = new ModelRenderer(this);
		234.setRotationPoint(-8.0F, -8.0F, 8.0F);
		everything.addChild(234);
		234.setTextureOffset(8, 12).addBox(-1.0F, -10.6F, 1.9F, 2.0F, 0.0F, 9.0F, 0.0F, false);
		234.setTextureOffset(8, 12).addBox(-1.0F, -10.3F, 10.7F, 2.0F, 12.0F, 0.0F, 0.0F, false);
		234.setTextureOffset(8, 12).addBox(-1.1F, -0.3F, 10.7F, 2.0F, 2.0F, 0.0F, 0.0F, false);
		234.setTextureOffset(5, 12).addBox(-2.4F, 0.3F, 10.85F, 0.0F, 0.0F, 0.0F, 0.0F, false);
		234.setTextureOffset(5, 12).addBox(-4.5F, 0.3F, 10.85F, 0.0F, 0.0F, 0.0F, 0.0F, false);
		234.setTextureOffset(5, 12).addBox(1.8F, 0.3F, 10.85F, 0.0F, 0.0F, 0.0F, 0.0F, false);
		234.setTextureOffset(8, 12).addBox(6.4F, -0.3F, -2.4F, 0.0F, 2.0F, 13.0F, 0.0F, false);
		234.setTextureOffset(8, 12).addBox(1.0F, -0.3F, 10.7F, 5.0F, 2.0F, 0.0F, 0.0F, false);
		234.setTextureOffset(8, 12).addBox(-6.7F, -0.3F, 10.7F, 5.0F, 2.0F, 0.0F, 0.0F, false);
		234.setTextureOffset(8, 12).addBox(-6.7F, -0.3F, -2.4F, 0.0F, 2.0F, 13.0F, 0.0F, false);
		234.setTextureOffset(8, 12).addBox(-1.0F, -10.725F, 2.35F, 2.0F, 0.0F, 1.0F, 0.0F, false);
		234.setTextureOffset(8, 12).addBox(-1.0F, -11.075F, 2.05F, 2.0F, 0.0F, 1.0F, 0.0F, false);
		234.setTextureOffset(8, 12).addBox(-6.4F, -0.3F, -2.4F, 12.0F, 2.0F, 0.0F, 0.0F, false);

		cube_r22 = new ModelRenderer(this);
		cube_r22.setRotationPoint(-7.25F, -10.05F, 2.45F);
		234.addChild(cube_r22);
		setRotationAngle(cube_r22, -0.7854F, 0.0F, 0.0F);
		cube_r22.setTextureOffset(5, 11).addBox(-0.15F, -0.25F, -0.25F, 0.0F, 0.0F, 0.0F, 0.0F, false);
		cube_r22.setTextureOffset(5, 11).addBox(14.45F, -0.25F, -0.25F, 0.0F, 0.0F, 0.0F, 0.0F, false);

		cube_r23 = new ModelRenderer(this);
		cube_r23.setRotationPoint(-7.25F, -7.05F, 2.45F);
		234.addChild(cube_r23);
		setRotationAngle(cube_r23, -0.7854F, 0.0F, 0.0F);
		cube_r23.setTextureOffset(5, 11).addBox(-0.15F, -0.25F, -0.25F, 0.0F, 0.0F, 0.0F, 0.0F, false);
		cube_r23.setTextureOffset(5, 11).addBox(14.45F, -0.25F, -0.25F, 0.0F, 0.0F, 0.0F, 0.0F, false);

		cube_r24 = new ModelRenderer(this);
		cube_r24.setRotationPoint(-7.25F, -4.05F, 2.45F);
		234.addChild(cube_r24);
		setRotationAngle(cube_r24, -0.7854F, 0.0F, 0.0F);
		cube_r24.setTextureOffset(5, 11).addBox(-0.15F, -0.25F, -0.25F, 0.0F, 0.0F, 0.0F, 0.0F, false);
		cube_r24.setTextureOffset(5, 11).addBox(14.45F, -0.25F, -0.25F, 0.0F, 0.0F, 0.0F, 0.0F, false);

		cube_r25 = new ModelRenderer(this);
		cube_r25.setRotationPoint(-5.45F, -11.05F, 2.45F);
		234.addChild(cube_r25);
		setRotationAngle(cube_r25, 0.0F, 0.7854F, 0.0F);
		cube_r25.setTextureOffset(5, 11).addBox(-0.25F, -0.15F, -0.25F, 0.0F, 0.0F, 0.0F, 0.0F, false);

		cube_r26 = new ModelRenderer(this);
		cube_r26.setRotationPoint(-2.45F, -11.05F, 2.45F);
		234.addChild(cube_r26);
		setRotationAngle(cube_r26, 0.0F, 0.7854F, 0.0F);
		cube_r26.setTextureOffset(5, 11).addBox(-0.25F, -0.15F, -0.25F, 0.0F, 0.0F, 0.0F, 0.0F, false);

		cube_r27 = new ModelRenderer(this);
		cube_r27.setRotationPoint(2.55F, -11.05F, 2.45F);
		234.addChild(cube_r27);
		setRotationAngle(cube_r27, 0.0F, 0.7854F, 0.0F);
		cube_r27.setTextureOffset(5, 11).addBox(-0.25F, -0.15F, -0.25F, 0.0F, 0.0F, 0.0F, 0.0F, false);

		cube_r28 = new ModelRenderer(this);
		cube_r28.setRotationPoint(5.55F, -11.05F, 2.45F);
		234.addChild(cube_r28);
		setRotationAngle(cube_r28, 0.0F, 0.7854F, 0.0F);
		cube_r28.setTextureOffset(5, 11).addBox(-0.25F, -0.15F, -0.25F, 0.0F, 0.0F, 0.0F, 0.0F, false);

		cube_r29 = new ModelRenderer(this);
		cube_r29.setRotationPoint(0.55F, -11.05F, 2.45F);
		234.addChild(cube_r29);
		setRotationAngle(cube_r29, 0.0F, 0.7854F, 0.0F);
		cube_r29.setTextureOffset(5, 12).addBox(-0.15F, -0.05F, -0.15F, 0.0F, 0.0F, 0.0F, 0.0F, false);

		cube_r30 = new ModelRenderer(this);
		cube_r30.setRotationPoint(-0.55F, -11.05F, 2.45F);
		234.addChild(cube_r30);
		setRotationAngle(cube_r30, 0.0F, 0.7854F, 0.0F);
		cube_r30.setTextureOffset(5, 12).addBox(-0.15F, -0.05F, -0.15F, 0.0F, 0.0F, 0.0F, 0.0F, false);

		cube_r31 = new ModelRenderer(this);
		cube_r31.setRotationPoint(0.15F, 0.65F, 11.0F);
		234.addChild(cube_r31);
		setRotationAngle(cube_r31, 0.0F, 0.0F, 0.7854F);
		cube_r31.setTextureOffset(5, 12).addBox(-0.55F, -0.55F, -0.25F, 1.0F, 1.0F, 0.0F, 0.0F, false);

		cube_r32 = new ModelRenderer(this);
		cube_r32.setRotationPoint(0.0F, -10.5346F, 4.0627F);
		234.addChild(cube_r32);
		setRotationAngle(cube_r32, -0.3927F, 0.0F, 0.0F);
		cube_r32.setTextureOffset(8, 12).addBox(-1.0F, -0.15F, -1.05F, 2.0F, 0.0F, 1.0F, 0.0F, false);

		345 = new ModelRenderer(this);
		345.setRotationPoint(-8.0F, -8.0F, 8.0F);
		everything.addChild(345);
		345.setTextureOffset(4, 4).addBox(-7.235F, -11.0F, -3.0F, 14.0F, 1.0F, 6.0F, 0.0F, true);
		345.setTextureOffset(0, 6).addBox(-7.235F, -10.0F, -3.0F, 14.0F, 8.0F, 1.0F, 0.0F, true);
		345.setTextureOffset(5, 4).addBox(-7.235F, -10.0F, -2.0F, 0.0F, 8.0F, 5.0F, 0.0F, false);
		345.setTextureOffset(4, 4).addBox(6.565F, -10.0F, -2.0F, 0.0F, 8.0F, 5.0F, 0.0F, true);
		345.setTextureOffset(5, 12).addBox(-1.835F, -4.1F, -3.5F, 3.0F, 1.0F, 1.0F, 0.0F, false);

		cube_r33 = new ModelRenderer(this);
		cube_r33.setRotationPoint(1.0F, -2.7F, -1.3F);
		345.addChild(cube_r33);
		setRotationAngle(cube_r33, -0.7854F, 0.0F, 0.0F);
		cube_r33.setTextureOffset(0, 14).addBox(5.665F, -0.9F, -1.5F, 1.0F, 1.0F, 1.0F, 0.0F, false);
		cube_r33.setTextureOffset(0, 14).addBox(-8.735F, -0.9F, -1.5F, 1.0F, 1.0F, 1.0F, 0.0F, false);

		3453543 = new ModelRenderer(this);
		3453543.setRotationPoint(-2.8349F, -3.0F, -3.5972F);
		345.addChild(3453543);
		3453543.setTextureOffset(0, 2).addBox(6.1596F, -0.4F, -0.7547F, 0.0F, 1.0F, 2.0F, 0.0F, false);
		3453543.setTextureOffset(0, 4).addBox(6.1596F, -2.4F, -0.7547F, 0.0F, 1.0F, 2.0F, 0.0F, false);
		3453543.setTextureOffset(0, 4).addBox(-1.4404F, -2.4F, -0.7547F, 0.0F, 1.0F, 2.0F, 0.0F, false);
		3453543.setTextureOffset(0, 2).addBox(-1.4404F, -0.4F, -0.7547F, 0.0F, 1.0F, 2.0F, 0.0F, false);

		cube_r34 = new ModelRenderer(this);
		cube_r34.setRotationPoint(0.0F, 0.0F, 0.0F);
		3453543.addChild(cube_r34);
		setRotationAngle(cube_r34, 0.0F, -0.3927F, 0.0F);
		cube_r34.setTextureOffset(0, 5).addBox(0.235F, -2.4F, -0.5F, 0.0F, 0.0F, 2.0F, 0.0F, false);
		cube_r34.setTextureOffset(0, 2).addBox(-0.265F, -1.9F, -0.5F, 0.0F, 1.0F, 2.0F, 0.0F, false);
		cube_r34.setTextureOffset(0, 9).addBox(0.235F, 0.6F, -0.5F, 0.0F, 0.0F, 2.0F, 0.0F, false);
		cube_r34.setTextureOffset(0, 2).addBox(-0.265F, -0.4F, -0.5F, 0.0F, 1.0F, 2.0F, 0.0F, false);
		cube_r34.setTextureOffset(0, 9).addBox(-0.765F, 0.6F, -0.5F, 0.0F, 0.0F, 2.0F, 0.0F, false);
		cube_r34.setTextureOffset(0, 2).addBox(-0.765F, -1.4F, -0.5F, 0.0F, 1.0F, 2.0F, 0.0F, false);
		cube_r34.setTextureOffset(0, 4).addBox(-0.765F, -2.4F, -0.5F, 0.0F, 0.0F, 2.0F, 0.0F, false);
		cube_r34.setTextureOffset(0, 4).addBox(-0.265F, -2.4F, -0.5F, 0.0F, 0.0F, 2.0F, 0.0F, false);
		cube_r34.setTextureOffset(0, 2).addBox(0.235F, -1.4F, -0.5F, 0.0F, 1.0F, 2.0F, 0.0F, false);

		cube_r35 = new ModelRenderer(this);
		cube_r35.setRotationPoint(0.7012F, 0.0F, 0.7174F);
		3453543.addChild(cube_r35);
		setRotationAngle(cube_r35, 0.0F, 0.3927F, 0.0F);
		cube_r35.setTextureOffset(0, 4).addBox(-0.9F, -2.4F, -0.765F, 1.0F, 1.0F, 0.0F, 0.0F, false);
		cube_r35.setTextureOffset(0, 1).addBox(-0.9F, -0.4F, -0.765F, 1.0F, 1.0F, 0.0F, 0.0F, false);
		cube_r35.setTextureOffset(0, 1).addBox(-0.9F, -1.4F, -0.265F, 1.0F, 2.0F, 1.0F, 0.0F, false);
		cube_r35.setTextureOffset(0, 4).addBox(-0.9F, -2.4F, -0.265F, 1.0F, 0.0F, 1.0F, 0.0F, false);

		cube_r36 = new ModelRenderer(this);
		cube_r36.setRotationPoint(-2.4531F, 0.0F, 0.2278F);
		3453543.addChild(cube_r36);
		setRotationAngle(cube_r36, 0.0F, -0.3927F, 0.0F);
		cube_r36.setTextureOffset(1, 9).addBox(-0.5F, 0.6F, 0.265F, 1.0F, 0.0F, 1.0F, 0.0F, true);
		cube_r36.setTextureOffset(0, 1).addBox(-0.5F, -0.4F, -0.235F, 1.0F, 1.0F, 0.0F, 0.0F, true);
		cube_r36.setTextureOffset(1, 1).addBox(-0.5F, -1.4F, 0.265F, 1.0F, 1.0F, 1.0F, 0.0F, true);
		cube_r36.setTextureOffset(1, 9).addBox(-0.5F, -2.4F, 0.265F, 1.0F, 0.0F, 1.0F, 0.0F, true);
		cube_r36.setTextureOffset(0, 9).addBox(-0.5F, -2.4F, -0.235F, 1.0F, 1.0F, 0.0F, 0.0F, true);

		cube_r37 = new ModelRenderer(this);
		cube_r37.setRotationPoint(-1.4651F, 0.0F, -0.2028F);
		3453543.addChild(cube_r37);
		setRotationAngle(cube_r37, 0.0F, 0.3927F, 0.0F);
		cube_r37.setTextureOffset(0, 9).addBox(-1.265F, -2.4F, -0.5F, 0.0F, 0.0F, 2.0F, 0.0F, false);
		cube_r37.setTextureOffset(0, 2).addBox(-1.265F, -1.4F, -0.5F, 0.0F, 1.0F, 2.0F, 0.0F, false);
		cube_r37.setTextureOffset(0, 9).addBox(-1.265F, 0.6F, -0.5F, 0.0F, 0.0F, 2.0F, 0.0F, false);
		cube_r37.setTextureOffset(0, 4).addBox(-0.265F, -2.4F, -0.5F, 0.0F, 0.0F, 2.0F, 0.0F, false);
		cube_r37.setTextureOffset(0, 2).addBox(-0.265F, -1.4F, -0.5F, 0.0F, 1.0F, 2.0F, 0.0F, false);
		cube_r37.setTextureOffset(0, 9).addBox(-0.265F, 0.6F, -0.5F, 0.0F, 0.0F, 2.0F, 0.0F, false);
		cube_r37.setTextureOffset(0, 9).addBox(-0.765F, -2.4F, -0.5F, 0.0F, 0.0F, 2.0F, 0.0F, false);
		cube_r37.setTextureOffset(0, 2).addBox(-0.765F, -1.9F, -0.5F, 0.0F, 1.0F, 2.0F, 0.0F, false);
		cube_r37.setTextureOffset(0, 2).addBox(-0.765F, -0.4F, -0.5F, 0.0F, 1.0F, 2.0F, 0.0F, false);

		cube_r38 = new ModelRenderer(this);
		cube_r38.setRotationPoint(8.3012F, 0.0F, 0.7174F);
		3453543.addChild(cube_r38);
		setRotationAngle(cube_r38, 0.0F, 0.3927F, 0.0F);
		cube_r38.setTextureOffset(0, 5).addBox(-0.9F, -2.4F, -0.765F, 1.0F, 1.0F, 0.0F, 0.0F, false);
		cube_r38.setTextureOffset(0, 4).addBox(-0.9F, -2.4F, -0.265F, 1.0F, 0.0F, 1.0F, 0.0F, false);
		cube_r38.setTextureOffset(0, 1).addBox(-0.9F, -0.4F, -0.765F, 1.0F, 1.0F, 0.0F, 0.0F, false);
		cube_r38.setTextureOffset(0, 2).addBox(-0.9F, -1.4F, -0.265F, 1.0F, 1.0F, 1.0F, 0.0F, false);
		cube_r38.setTextureOffset(0, 9).addBox(-0.9F, 0.6F, -0.265F, 1.0F, 0.0F, 1.0F, 0.0F, false);

		cube_r39 = new ModelRenderer(this);
		cube_r39.setRotationPoint(7.6F, 0.0F, 0.0F);
		3453543.addChild(cube_r39);
		setRotationAngle(cube_r39, 0.0F, -0.3927F, 0.0F);
		cube_r39.setTextureOffset(0, 4).addBox(0.235F, -2.4F, -0.5F, 0.0F, 0.0F, 2.0F, 0.0F, false);
		cube_r39.setTextureOffset(0, 4).addBox(-0.265F, -2.4F, -0.5F, 0.0F, 0.0F, 2.0F, 0.0F, false);
		cube_r39.setTextureOffset(0, 4).addBox(-0.765F, -2.4F, -0.5F, 0.0F, 0.0F, 2.0F, 0.0F, false);
		cube_r39.setTextureOffset(0, 2).addBox(-0.265F, -1.9F, -0.5F, 0.0F, 1.0F, 2.0F, 0.0F, false);
		cube_r39.setTextureOffset(0, 2).addBox(0.235F, -1.4F, -0.5F, 0.0F, 1.0F, 2.0F, 0.0F, false);
		cube_r39.setTextureOffset(0, 10).addBox(0.235F, 0.6F, -0.5F, 0.0F, 0.0F, 2.0F, 0.0F, false);
		cube_r39.setTextureOffset(0, 2).addBox(-0.265F, -0.4F, -0.5F, 0.0F, 1.0F, 2.0F, 0.0F, false);
		cube_r39.setTextureOffset(0, 10).addBox(-0.765F, 0.6F, -0.5F, 0.0F, 0.0F, 2.0F, 0.0F, false);
		cube_r39.setTextureOffset(0, 2).addBox(-0.765F, -1.4F, -0.5F, 0.0F, 1.0F, 2.0F, 0.0F, false);

		cube_r40 = new ModelRenderer(this);
		cube_r40.setRotationPoint(6.1349F, 0.0F, -0.2028F);
		3453543.addChild(cube_r40);
		setRotationAngle(cube_r40, 0.0F, 0.3927F, 0.0F);
		cube_r40.setTextureOffset(0, 4).addBox(-0.265F, -2.4F, -0.5F, 0.0F, 0.0F, 2.0F, 0.0F, false);
		cube_r40.setTextureOffset(0, 10).addBox(-0.765F, -2.4F, -0.5F, 0.0F, 0.0F, 2.0F, 0.0F, false);
		cube_r40.setTextureOffset(0, 9).addBox(-1.265F, -2.4F, -0.5F, 0.0F, 0.0F, 2.0F, 0.0F, false);
		cube_r40.setTextureOffset(0, 9).addBox(-1.265F, 0.6F, -0.5F, 0.0F, 0.0F, 2.0F, 0.0F, false);
		cube_r40.setTextureOffset(0, 2).addBox(-0.765F, -0.4F, -0.5F, 0.0F, 1.0F, 2.0F, 0.0F, false);
		cube_r40.setTextureOffset(0, 9).addBox(-0.265F, 0.6F, -0.5F, 0.0F, 0.0F, 2.0F, 0.0F, false);
		cube_r40.setTextureOffset(0, 2).addBox(-0.265F, -1.4F, -0.5F, 0.0F, 1.0F, 2.0F, 0.0F, false);
		cube_r40.setTextureOffset(0, 2).addBox(-0.765F, -1.9F, -0.5F, 0.0F, 1.0F, 2.0F, 0.0F, false);
		cube_r40.setTextureOffset(0, 2).addBox(-1.265F, -1.4F, -0.5F, 0.0F, 1.0F, 2.0F, 0.0F, false);

		cube_r41 = new ModelRenderer(this);
		cube_r41.setRotationPoint(5.1469F, 0.0F, 0.2278F);
		3453543.addChild(cube_r41);
		setRotationAngle(cube_r41, 0.0F, -0.3927F, 0.0F);
		cube_r41.setTextureOffset(0, 10).addBox(-0.5F, -2.4F, -0.235F, 1.0F, 1.0F, 0.0F, 0.0F, true);
		cube_r41.setTextureOffset(1, 9).addBox(-0.5F, -2.4F, 0.265F, 1.0F, 0.0F, 1.0F, 0.0F, true);
		cube_r41.setTextureOffset(1, 1).addBox(-0.5F, -1.4F, 0.265F, 1.0F, 1.0F, 1.0F, 0.0F, true);
		cube_r41.setTextureOffset(1, 9).addBox(-0.5F, 0.6F, 0.265F, 1.0F, 0.0F, 1.0F, 0.0F, true);
		cube_r41.setTextureOffset(0, 1).addBox(-0.5F, -0.4F, -0.235F, 1.0F, 1.0F, 0.0F, 0.0F, true);

		bone2 = new ModelRenderer(this);
		bone2.setRotationPoint(0.0F, 0.0F, 0.0F);
		345.addChild(bone2);
		
	}

	@Override
	public void setRotationAngles(Entity entity, float limbSwing, float limbSwingAmount, float ageInTicks,
			float netHeadYaw, float headPitch) {
		// previously the render function, render code was moved to a method below
	}

	@Override
	public void render(MatrixStack matrixStack, IVertexBuilder buffer, int packedLight, int packedOverlay, float red,
			float green, float blue, float alpha) {
		everything.render(matrixStack, buffer, packedLight, packedOverlay);
	}

	public void setRotationAngle(ModelRenderer modelRenderer, float x, float y, float z) {
		modelRenderer.rotateAngleX = x;
		modelRenderer.rotateAngleY = y;
		modelRenderer.rotateAngleZ = z;
	}
}