// Made with Blockbench 3.7.4
// Exported for Minecraft version 1.15
// Paste this class into your mod and generate all required imports

public static class ModelSwamp_mask2 extends EntityModel<Entity> {
	private final ModelRenderer everything;
	private final ModelRenderer cube_r1;
	private final ModelRenderer cube_r2;
	private final ModelRenderer cube_r3;
	private final ModelRenderer cube_r4;
	private final ModelRenderer cube_r5;
	private final ModelRenderer cube_r6;
	private final ModelRenderer cube_r7;
	private final ModelRenderer cube_r8;
	private final ModelRenderer cube_r9;
	private final ModelRenderer cube_r10;
	private final ModelRenderer cube_r11;
	private final ModelRenderer cube_r12;
	private final ModelRenderer cube_r13;
	private final ModelRenderer cube_r14;
	private final ModelRenderer cube_r15;
	private final ModelRenderer cube_r16;
	private final ModelRenderer cube_r17;
	private final ModelRenderer cube_r18;
	private final ModelRenderer cube_r19;
	private final ModelRenderer cube_r20;
	private final ModelRenderer cube_r21;
	private final ModelRenderer cube_r22;
	private final ModelRenderer cube_r23;
	private final ModelRenderer cube_r24;
	private final ModelRenderer cube_r25;
	private final ModelRenderer cube_r26;
	private final ModelRenderer cube_r27;
	private final ModelRenderer cube_r28;
	private final ModelRenderer cube_r29;
	private final ModelRenderer cube_r30;
	private final ModelRenderer cube_r31;
	private final ModelRenderer cube_r32;
	private final ModelRenderer cube_r33;
	private final ModelRenderer cube_r34;
	private final ModelRenderer cube_r35;
	private final ModelRenderer cube_r36;
	private final ModelRenderer cube_r37;
	private final ModelRenderer cube_r38;
	private final ModelRenderer cube_r39;

	public ModelSwamp_mask2() {
		textureWidth = 16;
		textureHeight = 16;

		everything = new ModelRenderer(this);
		everything.setRotationPoint(0.0F, 24.0F, 0.0F);
		everything.setTextureOffset(0, 9).addBox(-13.9754F, -13.3F, 4.9981F, 4.0F, 3.0F, 0.0F, 0.0F, false);
		everything.setTextureOffset(0, 9).addBox(-6.3754F, -13.3F, 4.9981F, 4.0F, 3.0F, 0.0F, 0.0F, false);
		everything.setTextureOffset(0, 9).addBox(-12.9754F, -13.3F, 4.9481F, 2.0F, 3.0F, 0.0F, 0.0F, false);
		everything.setTextureOffset(0, 9).addBox(-5.3754F, -13.3F, 4.9481F, 2.0F, 3.0F, 0.0F, 0.0F, false);
		everything.setTextureOffset(12, 6).addBox(-8.9331F, -5.3978F, 3.0009F, 1.0F, 0.0F, 0.0F, 0.0F, true);
		everything.setTextureOffset(12, 6).addBox(-9.2331F, -7.8914F, 3.1509F, 2.0F, 2.0F, 0.0F, 0.0F, true);
		everything.setTextureOffset(12, 6).addBox(-8.9331F, -8.3164F, 3.0009F, 1.0F, 0.0F, 0.0F, 0.0F, true);
		everything.setTextureOffset(12, 6).addBox(-6.8064F, -7.5598F, 3.0009F, 0.0F, 1.0F, 0.0F, 0.0F, true);
		everything.setTextureOffset(12, 6).addBox(-9.7073F, -7.5421F, 3.0009F, 0.0F, 1.0F, 0.0F, 0.0F, true);
		everything.setTextureOffset(5, 13).addBox(-10.135F, -8.608F, 3.3009F, 4.0F, 4.0F, 0.0F, 0.0F, false);
		everything.setTextureOffset(5, 13).addBox(-10.135F, -5.2872F, 5.2182F, 4.0F, 0.0F, 0.0F, 0.0F, false);
		everything.setTextureOffset(5, 13).addBox(-11.235F, -10.0F, 5.0F, 6.0F, 5.0F, 1.0F, 0.0F, false);
		everything.setTextureOffset(8, 12).addBox(-14.4F, -8.3F, 5.6F, 12.0F, 2.0F, 0.0F, 0.0F, false);
		everything.setTextureOffset(8, 12).addBox(-9.0F, -19.075F, 10.05F, 2.0F, 0.0F, 1.0F, 0.0F, false);
		everything.setTextureOffset(8, 12).addBox(-9.0F, -18.725F, 10.35F, 2.0F, 0.0F, 1.0F, 0.0F, false);
		everything.setTextureOffset(8, 12).addBox(-14.7F, -8.3F, 5.6F, 0.0F, 2.0F, 13.0F, 0.0F, false);
		everything.setTextureOffset(8, 12).addBox(-14.7F, -8.3F, 18.7F, 5.0F, 2.0F, 0.0F, 0.0F, false);
		everything.setTextureOffset(8, 12).addBox(-7.0F, -8.3F, 18.7F, 5.0F, 2.0F, 0.0F, 0.0F, false);
		everything.setTextureOffset(8, 12).addBox(-1.6F, -8.3F, 5.6F, 0.0F, 2.0F, 13.0F, 0.0F, false);
		everything.setTextureOffset(5, 12).addBox(-6.2F, -7.7F, 18.85F, 0.0F, 0.0F, 0.0F, 0.0F, false);
		everything.setTextureOffset(5, 12).addBox(-12.5F, -7.7F, 18.85F, 0.0F, 0.0F, 0.0F, 0.0F, false);
		everything.setTextureOffset(5, 12).addBox(-10.4F, -7.7F, 18.85F, 0.0F, 0.0F, 0.0F, 0.0F, false);
		everything.setTextureOffset(8, 12).addBox(-9.1F, -8.3F, 18.7F, 2.0F, 2.0F, 0.0F, 0.0F, false);
		everything.setTextureOffset(8, 12).addBox(-9.0F, -18.3F, 18.7F, 2.0F, 12.0F, 0.0F, 0.0F, false);
		everything.setTextureOffset(8, 12).addBox(-9.0F, -18.6F, 9.9F, 2.0F, 0.0F, 9.0F, 0.0F, false);
		everything.setTextureOffset(5, 12).addBox(-9.835F, -12.1F, 4.5F, 3.0F, 1.0F, 1.0F, 0.0F, false);
		everything.setTextureOffset(4, 4).addBox(-1.435F, -18.0F, 6.0F, 0.0F, 8.0F, 5.0F, 0.0F, true);
		everything.setTextureOffset(5, 4).addBox(-15.235F, -18.0F, 6.0F, 0.0F, 8.0F, 5.0F, 0.0F, false);
		everything.setTextureOffset(0, 6).addBox(-15.235F, -18.0F, 5.0F, 14.0F, 8.0F, 1.0F, 0.0F, true);
		everything.setTextureOffset(4, 4).addBox(-15.235F, -19.0F, 5.0F, 14.0F, 1.0F, 6.0F, 0.0F, true);
		everything.setTextureOffset(0, 2).addBox(-12.2754F, -11.4F, 3.6481F, 0.0F, 1.0F, 2.0F, 0.0F, false);
		everything.setTextureOffset(0, 4).addBox(-12.2754F, -13.4F, 3.6481F, 0.0F, 1.0F, 2.0F, 0.0F, false);
		everything.setTextureOffset(0, 4).addBox(-4.6754F, -13.4F, 3.6481F, 0.0F, 1.0F, 2.0F, 0.0F, false);
		everything.setTextureOffset(0, 2).addBox(-4.6754F, -11.4F, 3.6481F, 0.0F, 1.0F, 2.0F, 0.0F, false);

		cube_r1 = new ModelRenderer(this);
		cube_r1.setRotationPoint(-3.2349F, -11.0F, 4.4028F);
		everything.addChild(cube_r1);
		setRotationAngle(cube_r1, 0.0F, -0.3927F, 0.0F);
		cube_r1.setTextureOffset(0, 2).addBox(-0.765F, -1.4F, -0.5F, 0.0F, 1.0F, 2.0F, 0.0F, false);
		cube_r1.setTextureOffset(0, 10).addBox(-0.765F, 0.6F, -0.5F, 0.0F, 0.0F, 2.0F, 0.0F, false);
		cube_r1.setTextureOffset(0, 2).addBox(-0.265F, -0.4F, -0.5F, 0.0F, 1.0F, 2.0F, 0.0F, false);
		cube_r1.setTextureOffset(0, 10).addBox(0.235F, 0.6F, -0.5F, 0.0F, 0.0F, 2.0F, 0.0F, false);
		cube_r1.setTextureOffset(0, 2).addBox(0.235F, -1.4F, -0.5F, 0.0F, 1.0F, 2.0F, 0.0F, false);
		cube_r1.setTextureOffset(0, 2).addBox(-0.265F, -1.9F, -0.5F, 0.0F, 1.0F, 2.0F, 0.0F, false);
		cube_r1.setTextureOffset(0, 4).addBox(-0.765F, -2.4F, -0.5F, 0.0F, 0.0F, 2.0F, 0.0F, false);
		cube_r1.setTextureOffset(0, 4).addBox(-0.265F, -2.4F, -0.5F, 0.0F, 0.0F, 2.0F, 0.0F, false);
		cube_r1.setTextureOffset(0, 4).addBox(0.235F, -2.4F, -0.5F, 0.0F, 0.0F, 2.0F, 0.0F, false);

		cube_r2 = new ModelRenderer(this);
		cube_r2.setRotationPoint(-2.5337F, -11.0F, 5.1203F);
		everything.addChild(cube_r2);
		setRotationAngle(cube_r2, 0.0F, 0.3927F, 0.0F);
		cube_r2.setTextureOffset(0, 9).addBox(-0.9F, 0.6F, -0.265F, 1.0F, 0.0F, 1.0F, 0.0F, false);
		cube_r2.setTextureOffset(0, 2).addBox(-0.9F, -1.4F, -0.265F, 1.0F, 1.0F, 1.0F, 0.0F, false);
		cube_r2.setTextureOffset(0, 1).addBox(-0.9F, -0.4F, -0.765F, 1.0F, 1.0F, 0.0F, 0.0F, false);
		cube_r2.setTextureOffset(0, 4).addBox(-0.9F, -2.4F, -0.265F, 1.0F, 0.0F, 1.0F, 0.0F, false);
		cube_r2.setTextureOffset(0, 5).addBox(-0.9F, -2.4F, -0.765F, 1.0F, 1.0F, 0.0F, 0.0F, false);

		cube_r3 = new ModelRenderer(this);
		cube_r3.setRotationPoint(-4.7F, -11.0F, 4.2F);
		everything.addChild(cube_r3);
		setRotationAngle(cube_r3, 0.0F, 0.3927F, 0.0F);
		cube_r3.setTextureOffset(0, 2).addBox(-1.265F, -1.4F, -0.5F, 0.0F, 1.0F, 2.0F, 0.0F, false);
		cube_r3.setTextureOffset(0, 2).addBox(-0.765F, -1.9F, -0.5F, 0.0F, 1.0F, 2.0F, 0.0F, false);
		cube_r3.setTextureOffset(0, 2).addBox(-0.265F, -1.4F, -0.5F, 0.0F, 1.0F, 2.0F, 0.0F, false);
		cube_r3.setTextureOffset(0, 9).addBox(-0.265F, 0.6F, -0.5F, 0.0F, 0.0F, 2.0F, 0.0F, false);
		cube_r3.setTextureOffset(0, 2).addBox(-0.765F, -0.4F, -0.5F, 0.0F, 1.0F, 2.0F, 0.0F, false);
		cube_r3.setTextureOffset(0, 9).addBox(-1.265F, 0.6F, -0.5F, 0.0F, 0.0F, 2.0F, 0.0F, false);
		cube_r3.setTextureOffset(0, 9).addBox(-1.265F, -2.4F, -0.5F, 0.0F, 0.0F, 2.0F, 0.0F, false);
		cube_r3.setTextureOffset(0, 10).addBox(-0.765F, -2.4F, -0.5F, 0.0F, 0.0F, 2.0F, 0.0F, false);
		cube_r3.setTextureOffset(0, 4).addBox(-0.265F, -2.4F, -0.5F, 0.0F, 0.0F, 2.0F, 0.0F, false);

		cube_r4 = new ModelRenderer(this);
		cube_r4.setRotationPoint(-5.688F, -11.0F, 4.6306F);
		everything.addChild(cube_r4);
		setRotationAngle(cube_r4, 0.0F, -0.3927F, 0.0F);
		cube_r4.setTextureOffset(0, 1).addBox(-0.5F, -0.4F, -0.235F, 1.0F, 1.0F, 0.0F, 0.0F, true);
		cube_r4.setTextureOffset(1, 9).addBox(-0.5F, 0.6F, 0.265F, 1.0F, 0.0F, 1.0F, 0.0F, true);
		cube_r4.setTextureOffset(1, 1).addBox(-0.5F, -1.4F, 0.265F, 1.0F, 1.0F, 1.0F, 0.0F, true);
		cube_r4.setTextureOffset(1, 9).addBox(-0.5F, -2.4F, 0.265F, 1.0F, 0.0F, 1.0F, 0.0F, true);
		cube_r4.setTextureOffset(0, 10).addBox(-0.5F, -2.4F, -0.235F, 1.0F, 1.0F, 0.0F, 0.0F, true);

		cube_r5 = new ModelRenderer(this);
		cube_r5.setRotationPoint(-12.3F, -11.0F, 4.2F);
		everything.addChild(cube_r5);
		setRotationAngle(cube_r5, 0.0F, 0.3927F, 0.0F);
		cube_r5.setTextureOffset(0, 2).addBox(-0.765F, -0.4F, -0.5F, 0.0F, 1.0F, 2.0F, 0.0F, false);
		cube_r5.setTextureOffset(0, 2).addBox(-0.765F, -1.9F, -0.5F, 0.0F, 1.0F, 2.0F, 0.0F, false);
		cube_r5.setTextureOffset(0, 9).addBox(-0.765F, -2.4F, -0.5F, 0.0F, 0.0F, 2.0F, 0.0F, false);
		cube_r5.setTextureOffset(0, 9).addBox(-0.265F, 0.6F, -0.5F, 0.0F, 0.0F, 2.0F, 0.0F, false);
		cube_r5.setTextureOffset(0, 2).addBox(-0.265F, -1.4F, -0.5F, 0.0F, 1.0F, 2.0F, 0.0F, false);
		cube_r5.setTextureOffset(0, 4).addBox(-0.265F, -2.4F, -0.5F, 0.0F, 0.0F, 2.0F, 0.0F, false);
		cube_r5.setTextureOffset(0, 9).addBox(-1.265F, 0.6F, -0.5F, 0.0F, 0.0F, 2.0F, 0.0F, false);
		cube_r5.setTextureOffset(0, 2).addBox(-1.265F, -1.4F, -0.5F, 0.0F, 1.0F, 2.0F, 0.0F, false);
		cube_r5.setTextureOffset(0, 9).addBox(-1.265F, -2.4F, -0.5F, 0.0F, 0.0F, 2.0F, 0.0F, false);

		cube_r6 = new ModelRenderer(this);
		cube_r6.setRotationPoint(-13.288F, -11.0F, 4.6306F);
		everything.addChild(cube_r6);
		setRotationAngle(cube_r6, 0.0F, -0.3927F, 0.0F);
		cube_r6.setTextureOffset(0, 9).addBox(-0.5F, -2.4F, -0.235F, 1.0F, 1.0F, 0.0F, 0.0F, true);
		cube_r6.setTextureOffset(1, 9).addBox(-0.5F, -2.4F, 0.265F, 1.0F, 0.0F, 1.0F, 0.0F, true);
		cube_r6.setTextureOffset(1, 1).addBox(-0.5F, -1.4F, 0.265F, 1.0F, 1.0F, 1.0F, 0.0F, true);
		cube_r6.setTextureOffset(0, 1).addBox(-0.5F, -0.4F, -0.235F, 1.0F, 1.0F, 0.0F, 0.0F, true);
		cube_r6.setTextureOffset(1, 9).addBox(-0.5F, 0.6F, 0.265F, 1.0F, 0.0F, 1.0F, 0.0F, true);

		cube_r7 = new ModelRenderer(this);
		cube_r7.setRotationPoint(-10.1337F, -11.0F, 5.1203F);
		everything.addChild(cube_r7);
		setRotationAngle(cube_r7, 0.0F, 0.3927F, 0.0F);
		cube_r7.setTextureOffset(0, 4).addBox(-0.9F, -2.4F, -0.265F, 1.0F, 0.0F, 1.0F, 0.0F, false);
		cube_r7.setTextureOffset(0, 1).addBox(-0.9F, -1.4F, -0.265F, 1.0F, 2.0F, 1.0F, 0.0F, false);
		cube_r7.setTextureOffset(0, 1).addBox(-0.9F, -0.4F, -0.765F, 1.0F, 1.0F, 0.0F, 0.0F, false);
		cube_r7.setTextureOffset(0, 4).addBox(-0.9F, -2.4F, -0.765F, 1.0F, 1.0F, 0.0F, 0.0F, false);

		cube_r8 = new ModelRenderer(this);
		cube_r8.setRotationPoint(-10.8349F, -11.0F, 4.4028F);
		everything.addChild(cube_r8);
		setRotationAngle(cube_r8, 0.0F, -0.3927F, 0.0F);
		cube_r8.setTextureOffset(0, 2).addBox(0.235F, -1.4F, -0.5F, 0.0F, 1.0F, 2.0F, 0.0F, false);
		cube_r8.setTextureOffset(0, 4).addBox(-0.265F, -2.4F, -0.5F, 0.0F, 0.0F, 2.0F, 0.0F, false);
		cube_r8.setTextureOffset(0, 4).addBox(-0.765F, -2.4F, -0.5F, 0.0F, 0.0F, 2.0F, 0.0F, false);
		cube_r8.setTextureOffset(0, 2).addBox(-0.765F, -1.4F, -0.5F, 0.0F, 1.0F, 2.0F, 0.0F, false);
		cube_r8.setTextureOffset(0, 9).addBox(-0.765F, 0.6F, -0.5F, 0.0F, 0.0F, 2.0F, 0.0F, false);
		cube_r8.setTextureOffset(0, 2).addBox(-0.265F, -0.4F, -0.5F, 0.0F, 1.0F, 2.0F, 0.0F, false);
		cube_r8.setTextureOffset(0, 9).addBox(0.235F, 0.6F, -0.5F, 0.0F, 0.0F, 2.0F, 0.0F, false);
		cube_r8.setTextureOffset(0, 2).addBox(-0.265F, -1.9F, -0.5F, 0.0F, 1.0F, 2.0F, 0.0F, false);
		cube_r8.setTextureOffset(0, 5).addBox(0.235F, -2.4F, -0.5F, 0.0F, 0.0F, 2.0F, 0.0F, false);

		cube_r9 = new ModelRenderer(this);
		cube_r9.setRotationPoint(-9.0F, -10.7F, 6.7F);
		everything.addChild(cube_r9);
		setRotationAngle(cube_r9, -0.7854F, 0.0F, 0.0F);
		cube_r9.setTextureOffset(0, 14).addBox(-6.735F, -0.9F, -1.5F, 1.0F, 1.0F, 1.0F, 0.0F, false);
		cube_r9.setTextureOffset(0, 14).addBox(7.665F, -0.9F, -1.5F, 1.0F, 1.0F, 1.0F, 0.0F, false);

		cube_r10 = new ModelRenderer(this);
		cube_r10.setRotationPoint(-8.0F, -18.5346F, 12.0627F);
		everything.addChild(cube_r10);
		setRotationAngle(cube_r10, -0.3927F, 0.0F, 0.0F);
		cube_r10.setTextureOffset(8, 12).addBox(-1.0F, -0.15F, -1.05F, 2.0F, 0.0F, 1.0F, 0.0F, false);

		cube_r11 = new ModelRenderer(this);
		cube_r11.setRotationPoint(-7.85F, -7.35F, 19.0F);
		everything.addChild(cube_r11);
		setRotationAngle(cube_r11, 0.0F, 0.0F, 0.7854F);
		cube_r11.setTextureOffset(5, 12).addBox(-0.55F, -0.55F, -0.25F, 1.0F, 1.0F, 0.0F, 0.0F, false);

		cube_r12 = new ModelRenderer(this);
		cube_r12.setRotationPoint(-8.55F, -19.05F, 10.45F);
		everything.addChild(cube_r12);
		setRotationAngle(cube_r12, 0.0F, 0.7854F, 0.0F);
		cube_r12.setTextureOffset(5, 12).addBox(-0.15F, -0.05F, -0.15F, 0.0F, 0.0F, 0.0F, 0.0F, false);

		cube_r13 = new ModelRenderer(this);
		cube_r13.setRotationPoint(-7.45F, -19.05F, 10.45F);
		everything.addChild(cube_r13);
		setRotationAngle(cube_r13, 0.0F, 0.7854F, 0.0F);
		cube_r13.setTextureOffset(5, 12).addBox(-0.15F, -0.05F, -0.15F, 0.0F, 0.0F, 0.0F, 0.0F, false);

		cube_r14 = new ModelRenderer(this);
		cube_r14.setRotationPoint(-0.75F, -12.05F, 10.45F);
		everything.addChild(cube_r14);
		setRotationAngle(cube_r14, -0.7854F, 0.0F, 0.0F);
		cube_r14.setTextureOffset(5, 11).addBox(-0.05F, -0.25F, -0.25F, 0.0F, 0.0F, 0.0F, 0.0F, false);
		cube_r14.setTextureOffset(5, 11).addBox(-14.65F, -0.25F, -0.25F, 0.0F, 0.0F, 0.0F, 0.0F, false);

		cube_r15 = new ModelRenderer(this);
		cube_r15.setRotationPoint(-0.75F, -15.05F, 10.45F);
		everything.addChild(cube_r15);
		setRotationAngle(cube_r15, -0.7854F, 0.0F, 0.0F);
		cube_r15.setTextureOffset(5, 11).addBox(-0.05F, -0.25F, -0.25F, 0.0F, 0.0F, 0.0F, 0.0F, false);
		cube_r15.setTextureOffset(5, 11).addBox(-14.65F, -0.25F, -0.25F, 0.0F, 0.0F, 0.0F, 0.0F, false);

		cube_r16 = new ModelRenderer(this);
		cube_r16.setRotationPoint(-0.75F, -18.05F, 10.45F);
		everything.addChild(cube_r16);
		setRotationAngle(cube_r16, -0.7854F, 0.0F, 0.0F);
		cube_r16.setTextureOffset(5, 11).addBox(-0.05F, -0.25F, -0.25F, 0.0F, 0.0F, 0.0F, 0.0F, false);
		cube_r16.setTextureOffset(5, 11).addBox(-14.65F, -0.25F, -0.25F, 0.0F, 0.0F, 0.0F, 0.0F, false);

		cube_r17 = new ModelRenderer(this);
		cube_r17.setRotationPoint(-2.45F, -19.05F, 10.45F);
		everything.addChild(cube_r17);
		setRotationAngle(cube_r17, 0.0F, 0.7854F, 0.0F);
		cube_r17.setTextureOffset(5, 11).addBox(-0.25F, -0.15F, -0.25F, 0.0F, 0.0F, 0.0F, 0.0F, false);

		cube_r18 = new ModelRenderer(this);
		cube_r18.setRotationPoint(-5.45F, -19.05F, 10.45F);
		everything.addChild(cube_r18);
		setRotationAngle(cube_r18, 0.0F, 0.7854F, 0.0F);
		cube_r18.setTextureOffset(5, 11).addBox(-0.25F, -0.15F, -0.25F, 0.0F, 0.0F, 0.0F, 0.0F, false);

		cube_r19 = new ModelRenderer(this);
		cube_r19.setRotationPoint(-10.45F, -19.05F, 10.45F);
		everything.addChild(cube_r19);
		setRotationAngle(cube_r19, 0.0F, 0.7854F, 0.0F);
		cube_r19.setTextureOffset(5, 11).addBox(-0.25F, -0.15F, -0.25F, 0.0F, 0.0F, 0.0F, 0.0F, false);

		cube_r20 = new ModelRenderer(this);
		cube_r20.setRotationPoint(-13.45F, -19.05F, 10.45F);
		everything.addChild(cube_r20);
		setRotationAngle(cube_r20, 0.0F, 0.7854F, 0.0F);
		cube_r20.setTextureOffset(5, 11).addBox(-0.25F, -0.15F, -0.25F, 0.0F, 0.0F, 0.0F, 0.0F, false);

		cube_r21 = new ModelRenderer(this);
		cube_r21.setRotationPoint(-6.3883F, -8.208F, 3.4847F);
		everything.addChild(cube_r21);
		setRotationAngle(cube_r21, 0.0F, -0.7854F, 0.0F);
		cube_r21.setTextureOffset(5, 13).addBox(0.24F, -0.4F, -0.5F, 2.0F, 3.0F, 2.0F, 0.0F, false);

		cube_r22 = new ModelRenderer(this);
		cube_r22.setRotationPoint(-11.6447F, -8.208F, 5.5176F);
		everything.addChild(cube_r22);
		setRotationAngle(cube_r22, 0.0F, -0.7854F, 0.0F);
		cube_r22.setTextureOffset(5, 13).addBox(-0.5F, -0.4F, -2.635F, 2.0F, 3.0F, 2.0F, 0.0F, false);

		cube_r23 = new ModelRenderer(this);
		cube_r23.setRotationPoint(-7.1005F, -8.4914F, 5.5F);
		everything.addChild(cube_r23);
		setRotationAngle(cube_r23, 0.0F, 0.0F, -0.3927F);
		cube_r23.setTextureOffset(5, 13).addBox(1.665F, -0.5F, -0.5F, 1.0F, 2.0F, 1.0F, 0.0F, false);

		cube_r24 = new ModelRenderer(this);
		cube_r24.setRotationPoint(-6.4499F, -8.0792F, 5.5F);
		everything.addChild(cube_r24);
		setRotationAngle(cube_r24, 0.0F, 0.0F, 0.3927F);
		cube_r24.setTextureOffset(5, 13).addBox(1.665F, -0.5F, -0.5F, 1.0F, 2.0F, 1.0F, 0.0F, false);

		cube_r25 = new ModelRenderer(this);
		cube_r25.setRotationPoint(-8.8995F, -8.4914F, 5.5F);
		everything.addChild(cube_r25);
		setRotationAngle(cube_r25, 0.0F, 0.0F, 0.3927F);
		cube_r25.setTextureOffset(5, 13).addBox(-2.735F, -0.5F, -0.5F, 1.0F, 2.0F, 1.0F, 0.0F, false);

		cube_r26 = new ModelRenderer(this);
		cube_r26.setRotationPoint(-9.5501F, -8.0792F, 5.5F);
		everything.addChild(cube_r26);
		setRotationAngle(cube_r26, 0.0F, 0.0F, -0.3927F);
		cube_r26.setTextureOffset(5, 13).addBox(-2.735F, -0.5F, -0.5F, 1.0F, 2.0F, 1.0F, 0.0F, false);

		cube_r27 = new ModelRenderer(this);
		cube_r27.setRotationPoint(-8.3921F, -6.4531F, 3.1509F);
		everything.addChild(cube_r27);
		setRotationAngle(cube_r27, 0.0F, 0.0F, 0.7854F);
		cube_r27.setTextureOffset(12, 6).addBox(-1.7F, -0.935F, -0.15F, 0.0F, 1.0F, 0.0F, 0.0F, true);

		cube_r28 = new ModelRenderer(this);
		cube_r28.setRotationPoint(-6.1191F, -4.1447F, 3.1509F);
		everything.addChild(cube_r28);
		setRotationAngle(cube_r28, 0.0F, 0.0F, 0.7854F);
		cube_r28.setTextureOffset(12, 6).addBox(-1.7F, -0.935F, -0.15F, 0.0F, 1.0F, 0.0F, 0.0F, true);

		cube_r29 = new ModelRenderer(this);
		cube_r29.setRotationPoint(-7.9978F, -6.6036F, 3.1509F);
		everything.addChild(cube_r29);
		setRotationAngle(cube_r29, 0.0F, 0.0F, -0.7854F);
		cube_r29.setTextureOffset(12, 6).addBox(-1.8675F, -0.5501F, -0.15F, 0.0F, 1.0F, 0.0F, 0.0F, true);

		cube_r30 = new ModelRenderer(this);
		cube_r30.setRotationPoint(-8.0071F, -6.6194F, 3.1509F);
		everything.addChild(cube_r30);
		setRotationAngle(cube_r30, 0.0F, 0.0F, -0.7854F);
		cube_r30.setTextureOffset(12, 6).addBox(1.3675F, -0.5324F, -0.15F, 0.0F, 1.0F, 0.0F, 0.0F, true);

		cube_r31 = new ModelRenderer(this);
		cube_r31.setRotationPoint(-8.5F, -3.6922F, 6.8182F);
		everything.addChild(cube_r31);
		setRotationAngle(cube_r31, -0.3927F, 0.0F, 0.0F);
		cube_r31.setTextureOffset(5, 13).addBox(-1.635F, -0.5F, -3.6F, 4.0F, 1.0F, 2.0F, 0.0F, false);

		cube_r32 = new ModelRenderer(this);
		cube_r32.setRotationPoint(-5.2574F, -4.9984F, 4.7859F);
		everything.addChild(cube_r32);
		setRotationAngle(cube_r32, 0.0F, 0.0F, -0.7854F);
		cube_r32.setTextureOffset(5, 13).addBox(-1.135F, -0.5F, -0.135F, 1.0F, 0.0F, 1.0F, 0.0F, false);

		cube_r33 = new ModelRenderer(this);
		cube_r33.setRotationPoint(-10.7426F, -4.9984F, 4.7859F);
		everything.addChild(cube_r33);
		setRotationAngle(cube_r33, 0.0F, 0.0F, 0.7854F);
		cube_r33.setTextureOffset(5, 13).addBox(-0.335F, -0.5F, -0.135F, 1.0F, 0.0F, 1.0F, 0.0F, false);

		cube_r34 = new ModelRenderer(this);
		cube_r34.setRotationPoint(-8.5F, -10.1F, 5.5F);
		everything.addChild(cube_r34);
		setRotationAngle(cube_r34, -0.7854F, 0.0F, 0.0F);
		cube_r34.setTextureOffset(5, 13).addBox(-1.635F, 0.1F, -0.5F, 4.0F, 2.0F, 1.0F, 0.0F, false);

		cube_r35 = new ModelRenderer(this);
		cube_r35.setRotationPoint(-6.3883F, -8.008F, 3.3347F);
		everything.addChild(cube_r35);
		setRotationAngle(cube_r35, 0.0F, -0.7854F, 0.0F);
		cube_r35.setTextureOffset(12, 6).addBox(0.635F, -0.1F, -1.0F, 1.0F, 1.0F, 2.0F, 0.0F, true);
		cube_r35.setTextureOffset(7, 10).addBox(-0.365F, -1.1F, -2.225F, 3.0F, 3.0F, 1.0F, 0.0F, false);

		cube_r36 = new ModelRenderer(this);
		cube_r36.setRotationPoint(-9.6117F, -8.008F, 3.3347F);
		everything.addChild(cube_r36);
		setRotationAngle(cube_r36, 0.0F, 0.7854F, 0.0F);
		cube_r36.setTextureOffset(12, 6).addBox(-2.335F, -0.1F, -1.0F, 1.0F, 1.0F, 2.0F, 0.0F, true);
		cube_r36.setTextureOffset(7, 10).addBox(-3.335F, -1.1F, -2.425F, 3.0F, 3.0F, 1.0F, 0.0F, false);
		cube_r36.setTextureOffset(14, 4).addBox(-3.435F, -1.2F, -2.4F, 3.0F, 3.0F, 0.0F, 0.0F, false);

		cube_r37 = new ModelRenderer(this);
		cube_r37.setRotationPoint(-12.4296F, -7.283F, 2.617F);
		everything.addChild(cube_r37);
		setRotationAngle(cube_r37, 0.0F, 0.7854F, 0.0F);
		cube_r37.setTextureOffset(14, 4).addBox(-1.85F, -1.825F, -0.1F, 3.0F, 0.0F, 0.0F, 0.0F, false);
		cube_r37.setTextureOffset(14, 4).addBox(-1.85F, 1.575F, -0.1F, 3.0F, 0.0F, 0.0F, 0.0F, false);
		cube_r37.setTextureOffset(14, 4).addBox(1.55F, -1.525F, -0.1F, 0.0F, 3.0F, 0.0F, 0.0F, false);
		cube_r37.setTextureOffset(14, 4).addBox(-1.85F, -1.525F, -0.1F, 0.0F, 3.0F, 0.0F, 0.0F, false);
		cube_r37.setTextureOffset(14, 4).addBox(-1.25F, -1.525F, -0.1F, 0.0F, 3.0F, 0.0F, 0.0F, false);
		cube_r37.setTextureOffset(14, 4).addBox(-0.65F, -1.525F, -0.1F, 0.0F, 3.0F, 0.0F, 0.0F, false);
		cube_r37.setTextureOffset(14, 4).addBox(0.45F, -1.525F, -0.1F, 0.0F, 3.0F, 0.0F, 0.0F, false);
		cube_r37.setTextureOffset(14, 4).addBox(1.05F, -1.525F, -0.1F, 0.0F, 3.0F, 0.0F, 0.0F, false);
		cube_r37.setTextureOffset(14, 4).addBox(-1.65F, -1.225F, -0.1F, 3.0F, 0.0F, 0.0F, 0.0F, false);
		cube_r37.setTextureOffset(14, 4).addBox(-1.65F, -0.625F, -0.1F, 3.0F, 0.0F, 0.0F, 0.0F, false);
		cube_r37.setTextureOffset(14, 4).addBox(-1.65F, 1.075F, -0.1F, 3.0F, 0.0F, 0.0F, 0.0F, false);
		cube_r37.setTextureOffset(14, 4).addBox(-1.65F, 0.475F, -0.1F, 3.0F, 0.0F, 0.0F, 0.0F, false);
		cube_r37.setTextureOffset(14, 4).addBox(-1.65F, -0.125F, -0.1F, 3.0F, 0.0F, 0.0F, 0.0F, false);
		cube_r37.setTextureOffset(14, 4).addBox(-0.05F, -1.525F, -0.1F, 0.0F, 3.0F, 0.0F, 0.0F, false);

		cube_r38 = new ModelRenderer(this);
		cube_r38.setRotationPoint(-6.3883F, -8.008F, 3.3347F);
		everything.addChild(cube_r38);
		setRotationAngle(cube_r38, 0.0F, -0.7854F, 0.0F);
		cube_r38.setTextureOffset(14, 4).addBox(-0.465F, -1.2F, -2.2F, 3.0F, 3.0F, 0.0F, 0.0F, false);

		cube_r39 = new ModelRenderer(this);
		cube_r39.setRotationPoint(-3.5704F, -7.283F, 2.617F);
		everything.addChild(cube_r39);
		setRotationAngle(cube_r39, 0.0F, -0.7854F, 0.0F);
		cube_r39.setTextureOffset(14, 4).addBox(-1.85F, -1.525F, 0.1F, 0.0F, 3.0F, 0.0F, 0.0F, false);
		cube_r39.setTextureOffset(14, 4).addBox(-1.85F, -1.825F, 0.1F, 3.0F, 0.0F, 0.0F, 0.0F, false);
		cube_r39.setTextureOffset(14, 4).addBox(1.55F, -1.525F, 0.1F, 0.0F, 3.0F, 0.0F, 0.0F, false);
		cube_r39.setTextureOffset(14, 4).addBox(-1.85F, 1.575F, 0.1F, 3.0F, 0.0F, 0.0F, 0.0F, false);
		cube_r39.setTextureOffset(14, 4).addBox(-1.55F, -1.225F, 0.1F, 3.0F, 0.0F, 0.0F, 0.0F, true);
		cube_r39.setTextureOffset(14, 4).addBox(1.05F, -1.525F, 0.1F, 0.0F, 3.0F, 0.0F, 0.0F, true);
		cube_r39.setTextureOffset(14, 4).addBox(-1.55F, -0.125F, 0.1F, 3.0F, 0.0F, 0.0F, 0.0F, true);
		cube_r39.setTextureOffset(14, 4).addBox(-1.55F, -0.625F, 0.1F, 3.0F, 0.0F, 0.0F, 0.0F, true);
		cube_r39.setTextureOffset(14, 4).addBox(-1.55F, 0.475F, 0.1F, 3.0F, 0.0F, 0.0F, 0.0F, true);
		cube_r39.setTextureOffset(14, 4).addBox(-1.55F, 1.075F, 0.1F, 3.0F, 0.0F, 0.0F, 0.0F, true);
		cube_r39.setTextureOffset(14, 4).addBox(0.45F, -1.525F, 0.1F, 0.0F, 3.0F, 0.0F, 0.0F, true);
		cube_r39.setTextureOffset(14, 4).addBox(-0.05F, -1.525F, 0.1F, 0.0F, 3.0F, 0.0F, 0.0F, true);
		cube_r39.setTextureOffset(14, 4).addBox(-0.65F, -1.525F, 0.1F, 0.0F, 3.0F, 0.0F, 0.0F, true);
		cube_r39.setTextureOffset(14, 4).addBox(-1.25F, -1.525F, 0.1F, 0.0F, 3.0F, 0.0F, 0.0F, true);
	}

	@Override
	public void setRotationAngles(Entity entity, float limbSwing, float limbSwingAmount, float ageInTicks,
			float netHeadYaw, float headPitch) {
		// previously the render function, render code was moved to a method below
	}

	@Override
	public void render(MatrixStack matrixStack, IVertexBuilder buffer, int packedLight, int packedOverlay, float red,
			float green, float blue, float alpha) {
		everything.render(matrixStack, buffer, packedLight, packedOverlay);
	}

	public void setRotationAngle(ModelRenderer modelRenderer, float x, float y, float z) {
		modelRenderer.rotateAngleX = x;
		modelRenderer.rotateAngleY = y;
		modelRenderer.rotateAngleZ = z;
	}
}